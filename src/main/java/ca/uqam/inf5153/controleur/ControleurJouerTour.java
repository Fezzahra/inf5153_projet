/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.controleur;

import ca.uqam.inf5153.domaine.jeu.Cellule;
import ca.uqam.inf5153.domaine.jeu.Partie;
import ca.uqam.inf5153.utils.Position;
import ca.uqam.inf5153.utils.Couleur;
import java.util.List;

public class ControleurJouerTour {

    private Partie partie;

    public ControleurJouerTour() {
        this.partie = Partie.getInstance();
    }

    public List<Cellule> ctlPlacerPion(Couleur couleur, Position position) {
        List<Cellule> listeCellule = partie.placerPion(couleur, position);
        return listeCellule;
    }

    public List<Cellule> ctlDemarrerPartie(Integer niveau) {

        List<Cellule> listeCellule = partie.demarrerPartie(niveau);

        return listeCellule;
    }

    public Couleur ctlAssigneCouleur() {

        Couleur couleur = partie.determinerCouleur();
        return couleur;
    }

    public List<Cellule> ctlRevevoirCoupIA() {
        Position position = new Position(-1, -1);
        List<Cellule> listeCellule = partie.placerPion(partie.getCouleurIA(), position);
        return listeCellule;
    }
    
    public boolean ctlVerifierFinPartie() {
        return partie.verifierFinPartie();
    }
}
