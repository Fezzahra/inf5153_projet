/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.controleur;

import ca.uqam.inf5153.domaine.jeu.Cellule;
import ca.uqam.inf5153.domaine.jeu.Coup;
import ca.uqam.inf5153.domaine.jeu.Partie;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class ControleurChargerPartie {

    private Partie partie;

    public ControleurChargerPartie() {
        this.partie = Partie.getInstance();
    }
    
    // cette fonction retourne le niveau de difficulté de la partie selectionnée 
    public int ctlChargerPartie(long idPartie) throws ParserConfigurationException, SAXException, IOException {
        ArrayList<List<Cellule>> coups = new ArrayList<List<Cellule>>();
        int niv;

        partie = partie.chargerPartie(idPartie);
        niv = partie.getNiv();
        for (int i = 0; i < partie.getCoupsExecutes().size(); i++) {
            Coup coup = partie.getCoupsExecutes().get(i);
            List<Cellule> cellules = partie.getOthellier().chargerCoup(coup);
            coups.add(cellules);
        }
        Object[] resultat = {coups, niv};
        return niv;
    }

    //la fonction ctlLirePartie retourne la liste de toutes les cellules executées
    public ArrayList<List<Cellule>> ctlLirePartie() {
        ArrayList<List<Cellule>> coups = new ArrayList<List<Cellule>>();
        for (int i = 0; i < partie.getCoupsExecutes().size(); i++) {
            Coup coup = partie.getCoupsExecutes().get(i);
            List<Cellule> cellules = partie.getOthellier().chargerCoup(coup);
            coups.add(cellules);
        }
        return coups;
    }

    private static FilenameFilter xmlFileFilter = new FilenameFilter() {
        public boolean accept(File dir, String name) {
            return name.endsWith(".xml");
        }
    };

    // cette fonction retourn la liste des fichiers qui correspond aux parties sauvegardées 
    public ArrayList<String> ctlGetListeFichiers() {
        ArrayList<String> liste = new ArrayList<String>();
        File repertoire = new File("src/resources");
        String[] files = repertoire.list(xmlFileFilter);
        for (int i = 0; i < files.length; i++) {
            Long nomFic = 0L;
            try {
                if (Long.parseLong(files[i].substring(0, files[i].length() - 4)) > -1);
                    liste.add(files[i].substring(0, files[i].length() - 4));

            } catch (NumberFormatException ex) {
            }

        }
        return liste;
    }
}
