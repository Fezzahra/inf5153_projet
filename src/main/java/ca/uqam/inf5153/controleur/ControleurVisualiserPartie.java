/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.controleur;

import ca.uqam.inf5153.domaine.jeu.Cellule;
import ca.uqam.inf5153.domaine.jeu.Partie;
import ca.uqam.inf5153.domaine.jeu.Coup;
import java.util.ArrayList;
import java.util.List;

public class ControleurVisualiserPartie {
    private Partie partie;
    
    public ControleurVisualiserPartie() {
        this.partie = Partie.getInstance();
    }
    
    public ArrayList<List<Cellule>> ctlVisualiserPartie() {
        ArrayList<List<Cellule>> coups= new ArrayList<List<Cellule>>();
        for (int i = 0; i < partie.getCoupsExecutes().size(); i++) {
            Coup coup = partie.getCoupsExecutes().get(i);
            List<Cellule> cellules = partie.getOthellier().chargerCoup(coup);
            coups.add(cellules);
        }
        return coups;
    }
    
    public Cellule defilerCellule(List<Cellule> listeCellules) {
        return new Cellule(); 
    }
}
