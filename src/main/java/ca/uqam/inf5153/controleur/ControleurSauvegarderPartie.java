/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.controleur;

import ca.uqam.inf5153.domaine.jeu.Partie;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class ControleurSauvegarderPartie {
    
    private Partie partie;

    public ControleurSauvegarderPartie() {
       this.partie = Partie.getInstance();
        
    }
     
     
    public boolean ctlSauvegarderPartie() throws ParserConfigurationException, SAXException, IOException, TransformerException{
        return partie.sauvegarderPartie();
    }
}
