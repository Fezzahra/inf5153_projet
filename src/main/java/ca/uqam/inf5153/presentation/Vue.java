/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.presentation;

import ca.uqam.inf5153.controleur.*;
import ca.uqam.inf5153.domaine.jeu.Cellule;
import ca.uqam.inf5153.utils.Couleur;
import ca.uqam.inf5153.utils.Position;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class Vue extends JPanel {

    public static ArrayList<JPanel> listeCellule;
    public static List<Cellule> cellulesDepart;
    public static ArrayList<List<Cellule>> coups;
    public static List<String> listeIdParties;
    public static Integer noCoup = 0;
    public static Integer indFinPartie = 0;
    public static Integer niveau;
    public static Integer nbBlanc;
    public static Integer nbNoir;
    public static ControleurJouerTour instJouerTour;
    public static ControleurSauvegarderPartie instSauvPartie;
    public static ControleurChargerPartie instChargerPartie;
    public static ControleurVisualiserPartie instVisualiserPartie;

    public static JPanel topPanel;
    public static JPanel eastPanel;
    public static JPanel southPanel;
    public static JPanel boardPanel;
    public static JPanel cell;
    public static JPanel panDebut;

    public static JTextArea texte = new JTextArea();
    public static ButtonGroup bg;
    public static JButton btnNouvellePartie;
    public static JButton btnSauvgarderPartie;
    public static JButton btnChargerPartie;
    public static JButton btnVisualiserPartie;
    public static JButton btnSuivant;
    public static Couleur couleur;
    public static JRadioButton debutant;
    public static JRadioButton avance;
    public static JLabel parties;
    public static JLabel niveauDif;
    public static JComboBox listeParties;

    public Vue() {
        super(new BorderLayout());
        setOpaque(true);
        initialiserVue();
    }

    private void initialiserVue() {
        intialiserBoutons();
        initialiserComboBox();

        initialiserTopPanel();
        initialiserEastPanel();
        initialiserSouthPanel();
        initialiserBoardPanel();

        initialiserControleurs();

        add(topPanel, BorderLayout.NORTH);
        add(eastPanel, BorderLayout.LINE_END);
        add(boardPanel, BorderLayout.CENTER);
        add(southPanel, BorderLayout.SOUTH);

    }

    private void initialiserTopPanel() {

        topPanel = new JPanel(new FlowLayout());
        btnSauvgarderPartie.setEnabled(false);
        btnVisualiserPartie.setEnabled(false);
        btnSuivant.setEnabled(false);

        niveauDif = new JLabel("Choisir le niveau de difficulté:");
        topPanel.add(niveauDif);

        bg = new ButtonGroup();
        bg.add(debutant);
        bg.add(avance);
        debutant.setSelected(true);
        topPanel.add(debutant);
        topPanel.add(avance);

        topPanel.add(parties);
        listeParties.setPreferredSize(new Dimension(200, 30));
        topPanel.add(listeParties);
        parties.setEnabled(false);
        listeParties.setEnabled(false);
    }

    private void initialiserEastPanel() {
        eastPanel = new JPanel(new GridLayout(5, 1));

        btnNouvellePartie.setPreferredSize(new Dimension(250, 30));
        btnSauvgarderPartie.setPreferredSize(new Dimension(250, 30));
        btnChargerPartie.setPreferredSize(new Dimension(250, 30));
        btnVisualiserPartie.setPreferredSize(new Dimension(250, 30));
        btnSuivant.setPreferredSize(new Dimension(125, 30));
        eastPanel.add(btnNouvellePartie);
        eastPanel.add(btnChargerPartie);
        eastPanel.add(btnSauvgarderPartie);
        eastPanel.add(btnVisualiserPartie);
        eastPanel.add(btnSuivant);
    }

    private void initialiserSouthPanel() {
        southPanel = new JPanel();

        texte.setPreferredSize(new Dimension(800, 30));
        southPanel.add(texte);
    }

    private void initialiserBoardPanel() {
        boardPanel = new JPanel(new GridLayout(8, 8));
        listeCellule = new ArrayList<JPanel>();

        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                cell = new JPanel(new BorderLayout());
                cell.setSize(50, 50);
                cell.setBackground(Color.GREEN);
                cell.setBorder(BorderFactory.createLineBorder(Color.BLACK));

                boardPanel.add(cell);
                listeCellule.add(cell);

                final Position position = new Position();
                position.setPosition(row, col);
                cell.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        placerPion(position);

                    }
                });

            }
        }
    }

    private void intialiserBoutons() {

        btnNouvellePartie = new JButton("Démarrer partie");
        btnSauvgarderPartie = new JButton("Sauvegarder partie");
        btnChargerPartie = new JButton("Charger partie");
        btnVisualiserPartie = new JButton("Visualiser Partie");
        btnSuivant = new JButton("Coup Suivant");
        debutant = new JRadioButton("Débutant");
        avance = new JRadioButton("Avancé");

        btnNouvellePartie.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnNouvellePartie.setEnabled(false);
                demarerNouvellePartie();
            }

        });

        btnSuivant.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnSuivant.setEnabled(false);
                afficherCoup();
            }

        });

        btnSauvgarderPartie.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnNouvellePartie.setEnabled(false);
                try {
                    sauvegarderPartie();
                } catch (ParserConfigurationException ex) {
                    Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SAXException ex) {
                    Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
                } catch (TransformerException ex) {
                    Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });

        btnChargerPartie.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnNouvellePartie.setEnabled(false);
                parties.setEnabled(true);
                listeParties.setEnabled(true);
                listeIdParties = instChargerPartie.ctlGetListeFichiers();
                for (int i = 0; i < listeIdParties.size(); i++) {
                    Date date = new Date(Long.parseLong(listeIdParties.get(i)));
                    listeParties.addItem(date);
                }
                listeParties.setSelectedIndex(listeIdParties.size() - 1);
            }

        });

        btnVisualiserPartie.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnNouvellePartie.setEnabled(false);
                visualiserPartie();
                btnSuivant.setEnabled(true);

            }
        });

    }

    private void initialiserControleurs() {
        instJouerTour = new ControleurJouerTour();
        instSauvPartie = new ControleurSauvegarderPartie();
        instChargerPartie = new ControleurChargerPartie();
        instVisualiserPartie = new ControleurVisualiserPartie();

    }

    private void affecterCellulesDepart(List<Cellule> liste) {
        couleur = instJouerTour.ctlAssigneCouleur();
        texte.append("Début de la partie...");
        for (int i = 0; i < cellulesDepart.size(); i++) {
            Cellule cell = cellulesDepart.get(i);

            panDebut = listeCellule.get((cell.getPosition().getLigne() * 8) + cell.getPosition().getColonne());
            if (cell.getCouleur() == Couleur.Blanc) {
                panDebut.setBackground(Color.WHITE);
            } else {
                panDebut.setBackground(Color.BLACK);

            }
        }

    }
    // cette fonction initialse le comboBox avec toutes les parties sauvegardeées auparavant
    private void initialiserComboBox() {
        parties = new JLabel("Parties sauvegardées:");
        listeParties = new JComboBox<String>();

        listeParties.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                btnChargerPartie.setEnabled(false);
                int item = listeParties.getSelectedIndex();
                long idPartie = Long.parseLong(listeIdParties.get(item));
                try {
                    chargerPartie(idPartie);
                } catch (ParserConfigurationException ex) {
                    Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SAXException ex) {
                    Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

    }

    private void demarerNouvellePartie() {

        if (avance.isSelected()) {
            niveau = 0;
        } else if (debutant.isSelected()) {
            niveau = 1;
        }
        cellulesDepart = instJouerTour.ctlDemarrerPartie(niveau);

        affecterCellulesDepart(cellulesDepart);

        btnSauvgarderPartie.setEnabled(true);
        btnChargerPartie.setEnabled(false);
        btnVisualiserPartie.setEnabled(false);
        btnSuivant.setEnabled(false);

    }

    private void placerPion(Position position) {
        listeParties.setEnabled(false);
        btnSauvgarderPartie.setEnabled(true);
        texte.setText("position : X = " + position.getLigne() + "  Y = " + position.getColonne());
        List<Cellule> cellules = instJouerTour.ctlPlacerPion(couleur, position);
        List<Cellule> cellulesIA;
        if (cellules.size() > 0) {
            indFinPartie = 0;
            for (int i = 0; i < cellules.size(); i++) {
                Cellule cell = cellules.get(i);

                JPanel panDebut = listeCellule.get((cell.getPosition().getLigne() * 8) + cell.getPosition().getColonne());
                if (cell.getCouleur() == Couleur.Blanc) {
                    panDebut.setBackground(Color.WHITE);
                } else {
                    panDebut.setBackground(Color.BLACK);
                }
            }
        } else {
            indFinPartie++;
            texte.setText("Coup invalide!");
        }
        cellulesIA = instJouerTour.ctlRevevoirCoupIA();
        if (cellulesIA.size() > 0) {
            indFinPartie = 0;
            for (int i = 0; i < cellulesIA.size(); i++) {
                Cellule cell = cellulesIA.get(i);

                JPanel panDebut = listeCellule.get((cell.getPosition().getLigne() * 8) + cell.getPosition().getColonne());
                if (cell.getCouleur() == Couleur.Blanc) {
                    panDebut.setBackground(Color.WHITE);
                } else {
                    panDebut.setBackground(Color.BLACK);
                }
            }
        } else {
            indFinPartie++;
        }

        if (indFinPartie > 0 && instJouerTour.ctlVerifierFinPartie()) {
            terminerPartie();
        } else {
            indFinPartie = 0;
        }
    }

    private void sauvegarderPartie() throws ParserConfigurationException, SAXException, IOException, TransformerException {
        if (instSauvPartie.ctlSauvegarderPartie()) {
            texte.setText("Partie sauvegardée");
            btnChargerPartie.setEnabled(true);
        } else {
            texte.setText("Erreur lors de la sauvegarde");
        }

    }

    private void chargerPartie(long idPartie) throws ParserConfigurationException, SAXException, IOException {
        texte.setText(idPartie + "");
        niveau = instChargerPartie.ctlChargerPartie(idPartie);
        if (niveau == 0) {
            avance.setSelected(true);
        } else {
            debutant.setSelected(true);
        }

        coups = instChargerPartie.ctlLirePartie();
        cellulesDepart = instJouerTour.ctlDemarrerPartie(niveau);
        couleur = instJouerTour.ctlAssigneCouleur();
        noCoup = 0;
        if (coups.isEmpty()) {
            texte.setText("Aucun coup disponible!");
        } else {
            for (int row = 0; row < 8; row++) {
                for (int col = 0; col < 8; col++) {
                    JPanel panDebut = listeCellule.get((row * 8) + col);
                    panDebut.setBackground(Color.GREEN);
                }
            }
            for (int i = 0; i < cellulesDepart.size(); i++) {
                Cellule cell = cellulesDepart.get(i);

                JPanel panDebut = listeCellule.get((cell.getPosition().getLigne() * 8) + cell.getPosition().getColonne());
                if (cell.getCouleur() == Couleur.Blanc) {
                    panDebut.setBackground(Color.WHITE);
                } else {
                    panDebut.setBackground(Color.BLACK);

                }
            }
            for (int k = 0; k < coups.size(); k++) {
                List<Cellule> coup = coups.get(k);
                for (int i = 0; i < coup.size(); i++) {
                    Cellule cell = coup.get(i);
                    panDebut = listeCellule.get((cell.getPosition().getLigne() * 8) + cell.getPosition().getColonne());
                    if (cell.getCouleur() == Couleur.Blanc) {
                        panDebut.setBackground(Color.WHITE);
                    } else {
                        panDebut.setBackground(Color.BLACK);
                    }
                }
                texte.setText("Partie chargée! Vous pouvez sélectionner une autre dans la liste déroulante.");

            }
        }
    }

    private void visualiserPartie() {
        texte.setText("Visualisation");
        coups = instVisualiserPartie.ctlVisualiserPartie();
        noCoup = 0;
        if (coups.isEmpty()) {
            texte.setText("Aucun coup disponible!");
        } else {

            for (int row = 0; row < 8; row++) {
                for (int col = 0; col < 8; col++) {
                    JPanel panDebut = listeCellule.get((row * 8) + col);
                    panDebut.setBackground(Color.GREEN);
                }
            }
            for (int i = 0; i < cellulesDepart.size(); i++) {
                Cellule cell = cellulesDepart.get(i);

                JPanel panDebut = listeCellule.get((cell.getPosition().getLigne() * 8) + cell.getPosition().getColonne());
                if (cell.getCouleur() == Couleur.Blanc) {
                    panDebut.setBackground(Color.WHITE);
                } else {
                    panDebut.setBackground(Color.BLACK);

                }
            }
        }
        btnSauvgarderPartie.setEnabled(true);
        btnChargerPartie.setEnabled(true);
    }

    private void terminerPartie() {
        btnVisualiserPartie.setEnabled(true);

        nbBlanc = 0;
        nbNoir = 0;
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (listeCellule.get((row * 8) + col).getBackground() == Color.WHITE) {
                    nbBlanc++;
                } else {
                    nbNoir++;
                }
            }
        }
        texte.setText("Partie terminée! Noirs = " + nbNoir + ", Blancs = " + nbBlanc);
    }

    private void afficherCoup() {
        if (noCoup < coups.size()) {
            List<Cellule> coup = coups.get(noCoup);
            for (int i = 0; i < coup.size(); i++) {
                Cellule cell = coup.get(i);
                JPanel panDebut = listeCellule.get((cell.getPosition().getLigne() * 8) + cell.getPosition().getColonne());
                if (cell.getCouleur() == Couleur.Blanc) {
                    panDebut.setBackground(Color.WHITE);
                } else {
                    panDebut.setBackground(Color.BLACK);
                }
            }
            noCoup++;
            btnSuivant.setEnabled(true);
        } else {
            texte.setText("Plus de coup disponible");
            btnSuivant.setEnabled(false);
        }
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("Othello");
        frame.setSize(800, 600);
        frame.getContentPane().add(new Vue());
        frame.setLocationRelativeTo(null);
        frame.setBackground(Color.LIGHT_GRAY);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
