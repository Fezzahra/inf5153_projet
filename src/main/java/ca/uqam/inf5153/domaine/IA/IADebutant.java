/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.IA;

import ca.uqam.inf5153.domaine.jeu.Cellule;
import ca.uqam.inf5153.domaine.jeu.Othellier;
import ca.uqam.inf5153.utils.Couleur;
import ca.uqam.inf5153.utils.Position;
import java.util.List;
import java.util.Random;


public class IADebutant implements IAlgoIA {
    public IADebutant() {
        
    }
    // cette fonction genere une position au hasard
    public Position genererPosition(Othellier othellier, Couleur couleur) {
        Position position = new Position();
        List<Cellule> liste = othellier.getCoupsPossibles(couleur);
        if (liste.size() > 0) {
            int minimum = 0;
            int maximum = liste.size() - 1;
            Random rand = new Random();
            int randomNum = minimum + rand.nextInt((maximum - minimum) + 1);
            int ligne = liste.get(randomNum).getPosition().getLigne();
            int col = liste.get(randomNum).getPosition().getColonne();
            position.setPosition(ligne, col);
        } else {
            position.setPosition(-1, -1);
        }
        return position;
    }
    
}
