/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.IA;
import ca.uqam.inf5153.utils.Position;
import ca.uqam.inf5153.domaine.jeu.*;
import ca.uqam.inf5153.utils.Couleur;

public interface IAlgoIA {
    public Position genererPosition(Othellier othellier, Couleur couleur);
}
