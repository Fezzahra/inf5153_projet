/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.IA;

import ca.uqam.inf5153.domaine.jeu.Cellule;
import ca.uqam.inf5153.domaine.jeu.Othellier;
import ca.uqam.inf5153.utils.Couleur;
import ca.uqam.inf5153.utils.Direction;
import ca.uqam.inf5153.utils.Position;
import ca.uqam.inf5153.utils.Statut;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class IAAvance implements IAlgoIA {
    
    // cette fonction genere une position qui donnera le plus de pions.
    public Position genererPosition(Othellier othellier, Couleur couleur) {
        
        List<ArrayList<Cellule>> tableau = othellier.getTabCellules();
        Position position = new Position();
        position.setPosition(-1, -1);
        List<Cellule> liste = othellier.getCoupsPossibles(couleur);
        int nbPionsMax = 0;
        for (int ind = 0; ind < liste.size(); ind++) {
            Cellule cellule = liste.get(ind);
            int nbPionsCoup = 0;

            int ligne = cellule.getPosition().getLigne();
            int col = cellule.getPosition().getColonne();
            Couleur coulAdj;
            int minX = (ligne - 1 < 0 ? 0 : ligne - 1);
            int maxX = (ligne + 2 > 8 ? 8 : ligne + 2);
            int minY = (col - 1 < 0 ? 0 : col - 1);
            int maxY = (col + 2 > 8 ? 8 : col + 2);
            for (int i = minX; i < maxX; i++) {
                for (int j = minY; j < maxY; j++) {
                    if (tableau.get(i).get(j).getStatut() == Statut.Occupe &&
                            tableau.get(i).get(j).getCouleur() != couleur) {
                        coulAdj = tableau.get(i).get(j).getCouleur();
                        int ligSuiv = i;
                        int colSuiv = j;
                        if (i < ligne && j == col) {
                            nbPionsCoup = nbPionsCoup + othellier.caseSuivValide(Direction.N,ligSuiv, colSuiv, coulAdj);
                        }
                        if (i < ligne && j > col) {
                             nbPionsCoup = nbPionsCoup + othellier.caseSuivValide(Direction.NE, ligSuiv, colSuiv, coulAdj);
                        }
                        if (i == ligne && j > col) {
                           nbPionsCoup = nbPionsCoup + othellier.caseSuivValide(Direction.E, ligSuiv, colSuiv, coulAdj);
                        }
                        if (i > ligne && j > col) {
                            nbPionsCoup = nbPionsCoup + othellier.caseSuivValide(Direction.SE, ligSuiv, colSuiv, coulAdj);
                        }
                        if (i > ligne && j == col) {
                            nbPionsCoup = nbPionsCoup + othellier.caseSuivValide(Direction.S, ligSuiv, colSuiv, coulAdj);
                        }

                        if (i > ligne && j < col) {
                            nbPionsCoup = nbPionsCoup + othellier.caseSuivValide(Direction.SO, ligSuiv, colSuiv, coulAdj);
                        }

                        if (i  == ligne && j < col) {
                            nbPionsCoup = nbPionsCoup + othellier.caseSuivValide(Direction.O,ligSuiv, colSuiv, coulAdj);
                         }
                        if ( i < ligne && j < col) {
                            nbPionsCoup = nbPionsCoup + othellier.caseSuivValide(Direction.NO, ligSuiv, colSuiv, coulAdj);
                        }                   
                    }
                }
            }
 if (nbPionsCoup > nbPionsMax) {
                position.setPosition(cellule.getPosition().getLigne(), cellule.getPosition().getColonne());
                nbPionsMax = nbPionsCoup;
            }
        }

        return position;
    }
}
