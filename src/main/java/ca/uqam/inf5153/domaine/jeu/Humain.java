/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.jeu;


public class Humain extends Joueur {
    
    public Humain() {
        super();
    }
    public Humain(Integer id, String nom) {
        super(id, nom);
    }
    
}
