/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.jeu;

import ca.uqam.inf5153.utils.Couleur;


public class Joueur {
    Integer idJoueur;
    String nom;
    Couleur couleur;
    
     public Joueur() {
     
     }

    public Joueur(Integer id, String nom) {
        this.idJoueur = id;
        this.nom = nom;
    }
       
    
    public Couleur getCouleur() {
        return couleur;
    }
    
    public void setCouleur(Couleur couleur) {
        this.couleur=couleur;
    }
    
}
