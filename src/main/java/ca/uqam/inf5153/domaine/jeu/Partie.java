/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.jeu;

import ca.uqam.inf5153.utils.Position;
import ca.uqam.inf5153.domaine.IA.*;
import ca.uqam.inf5153.servicesTechniques.XMLPartie;
import ca.uqam.inf5153.utils.Couleur;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class Partie {

    private static Partie partie;
    private Othellier othellier;
    private Timestamp idPartie;
    private Integer niv;
    private IAlgoIA niveauDifficulte;
    private Joueur joueur1;
    private Joueur joueur2;
    private Joueur joueurCourant;
    private ArrayList<Coup> coupsExecutes;

    private Partie() {
        Date date = new java.util.Date();
        idPartie = new Timestamp(date.getTime());
        niv = 1;
        othellier = Othellier.getInstance();
        coupsExecutes = new ArrayList<Coup>();
        joueur1 = new Humain();
        joueur2 = new Ordinateur();
        joueur2.setCouleur(Couleur.Blanc);
        niveauDifficulte = choisirNiveauIA(niv);
    }

    @Override
    public String toString() {
        return "Partie{" + "othellier=" + othellier + ", idPartie=" + idPartie + ", niv=" + niv + '}';
    }

    public void setNiveauDifficulte(IAlgoIA niveauDifficulte) {
        this.niveauDifficulte = niveauDifficulte;
    }

    public static Partie getInstance() {
        if (partie == null) {
            partie = new Partie();
        }
        return partie;
    }

    public Othellier getOthellier() {
        return othellier;
    }

    public Integer getNiv() {
        return niv;
    }

    public Timestamp getId() {
        return idPartie;

    }

    public boolean sauvegarderPartie() throws ParserConfigurationException, SAXException, IOException, TransformerException {
        XMLPartie xmlPartie = new XMLPartie();
        ArrayList<List<Cellule>> coups = new ArrayList<List<Cellule>>();
        for (int i = 0; i < coupsExecutes.size(); i++) {
            Coup coup = coupsExecutes.get(i);
            List<Cellule> cellules = othellier.chargerCoup(coup);
            coups.add(cellules);
        }

        xmlPartie.asXML(coups, idPartie, niv);

        return true;
    }

    public Couleur getCouleurIA() {
        return joueur2.getCouleur();
    }

    public List<Cellule> placerPion(Couleur couleur, Position position) {
        List<Cellule> liste = new ArrayList<Cellule>();
        if (joueurCourant == joueur2) {
            Position pos = niveauDifficulte.genererPosition(othellier, getCouleurIA());
            if (pos.getLigne() < 0) {
                changerJoueurCourant();
                return liste;
            } else {
                position.setPosition(pos.getLigne(), pos.getColonne());
            }
        }
        if (peutJouer(couleur)) {
            Coup coup = othellier.creerCoup(couleur, position);
            if (coup != null) {
                liste = othellier.getCoupCellulesMAJ();
                coupsExecutes.add(coup);
                changerJoueurCourant();
            }
        }
        return liste;
    }

    public List<Cellule> demarrerPartie(Integer niveau) {
        this.niv = niveau;
        setNiveauDifficulte(choisirNiveauIA(niveau));
        List<Cellule> listeDeDepart = new ArrayList<Cellule>();
        listeDeDepart = othellier.cellulesDepart();
        joueurCourant = determinerPremierJoueur(joueur1, joueur2);
        return listeDeDepart;
    }

    public boolean peutJouer(Couleur couleur) {
        if (joueurCourant.getCouleur() == couleur) {
            return true;
        } else {
            return false;
        }

    }

    public void changerJoueurCourant() {
        if (joueurCourant == joueur1) {
            joueurCourant = joueur2;
        } else {
            joueurCourant = joueur1;
        }
    }

    public boolean verifierFinPartie() {
        List<Cellule> noirs = othellier.getCoupsPossibles(Couleur.Noir);
        List<Cellule> blancs = othellier.getCoupsPossibles(Couleur.Blanc);

        return noirs.size() == 0 && blancs.size() == 0;
    }

    public Partie chargerPartie(long idPartie) throws ParserConfigurationException, SAXException, IOException {
        othellier.initialiserTab();
        this.coupsExecutes = new ArrayList<Coup>();
        XMLPartie xmlPartie = new XMLPartie();
        xmlPartie.asPartie(idPartie);
        this.idPartie = new Timestamp(idPartie);
        this.niv = xmlPartie.getNiveau();
        ArrayList<ArrayList<Cellule>> coups = new ArrayList<ArrayList<Cellule>>();
        coups = xmlPartie.getCoupsExecutes();
        for (int i = 0; i < coups.size(); i++) {
            othellier.setCoupCellulesMAJ(coups.get(i));
            coupsExecutes.add(othellier.sauvegarderCoup());
        }

        return partie;
    }

    public void choisirTypeAdversaire(Joueur joueur) {
        joueur2 = joueur;
    }

    public List<Coup> getCoupsExecutes() {
        return coupsExecutes;
    }

    public IAlgoIA choisirNiveauIA(Integer niveau) {

        if (niveau == 1) {
            return new IADebutant();
        } else {
            return new IAAvance();
        }

    }

    private Joueur determinerPremierJoueur(Joueur joueur1, Joueur joueur2) {
        return joueur1; // À changer lors de l'implémentation
    }

    public Couleur determinerCouleur() {
        joueur1.setCouleur(Couleur.Noir);
        return Couleur.Noir;

    }
}
