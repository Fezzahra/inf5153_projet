/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.jeu;

import ca.uqam.inf5153.utils.Position;
import ca.uqam.inf5153.utils.*;
import java.util.ArrayList;
import java.util.List;

public class Othellier {

    private static Othellier othellier;
    private Integer noCoup;
    private ArrayList<ArrayList<Cellule>> tabCellules;
    private List<Cellule> coupCellulesMAJ;

    private Othellier() {
        noCoup = 0;
        tabCellules = new ArrayList<ArrayList<Cellule>>();
        coupCellulesMAJ = new ArrayList<Cellule>();
        for (int i = 0; i < 8; i++) {
            tabCellules.add(new ArrayList<Cellule>());
            for (int j = 0; j < 8; j++) {
                tabCellules.get(i).add(new Cellule(new Position(i, j)));
            }
        }
    }
    
    // lors de la demarrage d'une partie on initialise le tableau avec les postions de depart
    public void initialiserTab() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                tabCellules.get(i).get(j).affecterCellule(null);
            }
        }
        tabCellules.get(3).get(3).affecterCellule(Couleur.Blanc);
        tabCellules.get(3).get(4).affecterCellule(Couleur.Noir);
        tabCellules.get(4).get(3).affecterCellule(Couleur.Noir);
        tabCellules.get(4).get(4).affecterCellule(Couleur.Blanc);
    }

    public static Othellier getInstance() {
        if (othellier == null) {
            othellier = new Othellier();
        }
        return othellier;
    }

    public List<ArrayList<Cellule>> getTabCellules() {
        return tabCellules;
    }

    public Integer getNoCoup() {
        return noCoup;
    }

    public List<Cellule> cellulesDepart() {
        List<Cellule> cellulesDeDepart = new ArrayList<Cellule>();
        tabCellules.get(3).get(3).affecterCellule(Couleur.Blanc);
        tabCellules.get(3).get(4).affecterCellule(Couleur.Noir);
        tabCellules.get(4).get(3).affecterCellule(Couleur.Noir);
        tabCellules.get(4).get(4).affecterCellule(Couleur.Blanc);

        cellulesDeDepart.add(new Cellule(tabCellules.get(3).get(3)));
        cellulesDeDepart.add(new Cellule(tabCellules.get(3).get(4)));
        cellulesDeDepart.add(new Cellule(tabCellules.get(4).get(3)));
        cellulesDeDepart.add(new Cellule(tabCellules.get(4).get(4)));
        return cellulesDeDepart;
    }

    public List<Cellule> getCoupCellulesMAJ() {
        return coupCellulesMAJ;
    }

    public void setCoupCellulesMAJ(List<Cellule> cellules) {
        this.coupCellulesMAJ = cellules;
    }

    public List<Cellule> getCoupsPossibles(Couleur couleur) {
        List<Cellule> coupsPossibles = new ArrayList<Cellule>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (tabCellules.get(i).get(j).getStatut() == Statut.Vide) {
                    if (caseAdjValide(i, j, couleur)) {
                        Cellule cell = new Cellule(tabCellules.get(i).get(j));
                        cell.affecterCellule(couleur);
                        coupsPossibles.add(cell);
                    }
                }
            }
        }
        return coupsPossibles;
    }

    public void miseAjourCellules(Cellule cellule) {
        coupCellulesMAJ = new ArrayList<Cellule>();
        coupCellulesMAJ.add(new Cellule(cellule));

        int ligne = cellule.getPosition().getLigne();
        int col = cellule.getPosition().getColonne();
        Couleur couleur = cellule.getCouleur();
        Couleur coulAdj;
        int minX = (ligne - 1 < 0 ? 0 : ligne - 1);
        int maxX = (ligne + 2 > 8 ? 8 : ligne + 2);
        int minY = (col - 1 < 0 ? 0 : col - 1);
        int maxY = (col + 2 > 8 ? 8 : col + 2);
        for (int i = minX; i < maxX; i++) {
            for (int j = minY; j < maxY; j++) {
                if (tabCellules.get(i).get(j).getStatut() == Statut.Occupe
                        && tabCellules.get(i).get(j).getCouleur() != couleur) {
                    coulAdj = tabCellules.get(i).get(j).getCouleur();
                    int ligSuiv = i;
                    int colSuiv = j;
                    if (i < ligne && j == col) {
                        while (caseSuivValide(Direction.N, ligSuiv, colSuiv, coulAdj) > 0) {
                            renverserPion(tabCellules.get(ligSuiv).get(colSuiv), couleur);
                            ligSuiv--;
                        }
                    }
                    if (i < ligne && j > col) {
                        while (caseSuivValide(Direction.NE, ligSuiv, colSuiv, coulAdj) > 0) {
                            renverserPion(tabCellules.get(ligSuiv).get(colSuiv), couleur);
                            ligSuiv--;
                            colSuiv++;
                        }
                    }
                    if (i == ligne && j > col) {
                        while (caseSuivValide(Direction.E, ligSuiv, colSuiv, coulAdj) > 0) {
                            renverserPion(tabCellules.get(ligSuiv).get(colSuiv), couleur);
                            colSuiv++;
                        }
                    }
                    if (i > ligne && j > col) {
                        while (caseSuivValide(Direction.SE, ligSuiv, colSuiv, coulAdj) > 0) {
                            renverserPion(tabCellules.get(ligSuiv).get(colSuiv), couleur);
                            ligSuiv++;
                            colSuiv++;
                        }
                    }
                    if (i > ligne && j == col) {
                        while (caseSuivValide(Direction.S, ligSuiv, colSuiv, coulAdj) > 0) {
                            renverserPion(tabCellules.get(ligSuiv).get(colSuiv), couleur);
                            ligSuiv++;
                        }
                    }
                    if (i > ligne && j < col) {
                        while (caseSuivValide(Direction.SO, ligSuiv, colSuiv, coulAdj) > 0) {
                            renverserPion(tabCellules.get(ligSuiv).get(colSuiv), couleur);
                            ligSuiv++;
                            colSuiv--;
                        }
                    }
                    if (i == ligne && j < col) {
                        while (caseSuivValide(Direction.O, ligSuiv, colSuiv, coulAdj) > 0) {
                            renverserPion(tabCellules.get(ligSuiv).get(colSuiv), couleur);
                            colSuiv--;
                        }
                    }
                    if (i < ligne && j < col) {
                        while (caseSuivValide(Direction.NO, ligSuiv, colSuiv, coulAdj) > 0) {
                            renverserPion(tabCellules.get(ligSuiv).get(colSuiv), couleur);
                            ligSuiv--;
                            colSuiv--;
                        }
                    }
                }
            }
        }
    }

    private void renverserPion(Cellule cellule, Couleur couleur) {
        cellule.affecterCellule(couleur);
        coupCellulesMAJ.add(new Cellule(cellule));

    }

    public Coup creerCoup(Couleur couleur, Position position) {

        List<Cellule> liste = getCoupsPossibles(couleur);
        Coup coup = null;
        if (liste != null && liste.size() > 0) {
            for (int i = 0; i < liste.size(); i++) {
                if (liste.get(i).getPosition().getLigne() == position.getLigne()
                        && liste.get(i).getPosition().getColonne() == position.getColonne()) {

                    Cellule cellule = tabCellules.get(position.getLigne()).get(position.getColonne());
                    cellule.affecterCellule(couleur);
                    miseAjourCellules(cellule);
                    coup = sauvegarderCoup();
                    incrementerNoCoup();
                    break;
                }
            }
        }
        return coup;
    }

    public List<Cellule> chargerCoup(Coup coup) {
        setCoup(coup);
        return coupCellulesMAJ;
    }

    public Coup sauvegarderCoup() {
        return new Coup(noCoup, coupCellulesMAJ);
    }

    private void incrementerNoCoup() {
        noCoup++;
    }

    private void setCoup(Coup coup) {
        this.noCoup = coup.getNoCoup();
        this.coupCellulesMAJ = coup.getCoupCellulesMAJ();
        for (int i = 0; i < coupCellulesMAJ.size(); i++) {
            Cellule cellule = coupCellulesMAJ.get(i);
            int ligne = cellule.getPosition().getLigne();
            int col = cellule.getPosition().getColonne();
            tabCellules.get(ligne).get(col).affecterCellule(cellule.getCouleur());
        }
    }

    private boolean caseAdjValide(int ligne, int col, Couleur couleur) {
        Couleur coulAdj;
        int minX = (ligne - 1 < 0 ? 0 : ligne - 1);
        int maxX = (ligne + 2 > 8 ? 8 : ligne + 2);
        int minY = (col - 1 < 0 ? 0 : col - 1);
        int maxY = (col + 2 > 8 ? 8 : col + 2);
        for (int i = minX; i < maxX; i++) {
            for (int j = minY; j < maxY; j++) {
                if (tabCellules.get(i).get(j).getStatut() == Statut.Occupe
                        && tabCellules.get(i).get(j).getCouleur() != couleur) {
                    coulAdj = tabCellules.get(i).get(j).getCouleur();
                    if (i < ligne && j == col) {
                        if (caseSuivValide(Direction.N, i, j, coulAdj) > 0) {
                            return true;
                        }
                    }
                    if (i < ligne && j > col) {
                        if (caseSuivValide(Direction.NE, i, j, coulAdj) > 0) {
                            return true;
                        }
                    }
                    if (i == ligne && j > col) {
                        if (caseSuivValide(Direction.E, i, j, coulAdj) > 0) {
                            return true;
                        }
                    }
                    if (i > ligne && j > col) {
                        if (caseSuivValide(Direction.SE, i, j, coulAdj) > 0) {
                            return true;
                        }
                    }
                    if (i > ligne && j == col) {
                        if (caseSuivValide(Direction.S, i, j, coulAdj) > 0) {
                            return true;
                        }
                    }
                    if (i > ligne && j < col) {
                        if (caseSuivValide(Direction.SO, i, j, coulAdj) > 0) {
                            return true;
                        }
                    }

                    if (i == ligne && j < col) {
                        if (caseSuivValide(Direction.O, i, j, coulAdj) > 0) {
                            return true;
                        }
                    }
                    if (i < ligne && j < col) {
                        if (caseSuivValide(Direction.NO, i, j, coulAdj) > 0) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public int caseSuivValide(Direction dir, int i, int j, Couleur couleur) {
        int nbPion = 0;
        int ligSuiv = i;
        int colSuiv = j;
        boolean peutContinuer = false;

        switch (dir) {
            case N:
                ligSuiv = i - 1;
                colSuiv = j;
                peutContinuer = (i > 0);
                break;
            case S:
                ligSuiv = i + 1;
                colSuiv = j;
                peutContinuer = (i < 7);
                break;
            case E:
                ligSuiv = i;
                colSuiv = j + 1;
                peutContinuer = (j < 7);
                break;
            case O:
                ligSuiv = i;
                colSuiv = j - 1;
                peutContinuer = (j > 0);
                break;
            case NE:
                ligSuiv = i - 1;
                colSuiv = j + 1;
                peutContinuer = (i > 0 && j < 7);
                break;
            case SO:
                ligSuiv = i + 1;
                colSuiv = j - 1;
                peutContinuer = (i < 7 && j > 0);
                break;
            case SE:
                ligSuiv = i + 1;
                colSuiv = j + 1;
                peutContinuer = (i < 7 && j < 7);
                break;
            case NO:
                ligSuiv = i - 1;
                colSuiv = j - 1;
                peutContinuer = (i > 0 && j > 0);
                break;
        }

        if (peutContinuer) {
            if (tabCellules.get(ligSuiv).get(colSuiv).getStatut() == Statut.Occupe
                    && tabCellules.get(ligSuiv).get(colSuiv).getCouleur() != couleur) {
                nbPion = 1;
            } else {
                nbPion = nbPion + caseSuivValide(dir, ligSuiv, colSuiv, couleur);
            }
        }

        return nbPion;
    }
}
