/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.jeu;

import java.util.List;

public class Coup {

    private Integer noCoup;
    private List<Cellule> coupCellulesMAJ;

    public Coup() {
    }

    public Coup(Integer noCoup, List<Cellule> coupCellulesMAJ) {
        this.noCoup = noCoup;
        this.coupCellulesMAJ = coupCellulesMAJ;
    }

    public Integer getNoCoup() {
        return noCoup;
    }

    public List<Cellule> getCoupCellulesMAJ() {
        return coupCellulesMAJ;
    }

    @Override
    public String toString() {
        return "Coup{" + ", noCoup=" + noCoup + ", coupCellulesMAJ=" + coupCellulesMAJ + '}';
    }

}
