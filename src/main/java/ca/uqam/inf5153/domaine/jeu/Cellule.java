/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.domaine.jeu;

import ca.uqam.inf5153.utils.Position;
import ca.uqam.inf5153.utils.*;


public class Cellule {
    private Position position;
    private Statut statut;
    private Couleur couleur;

    public Cellule() {
        
    }
    public Cellule(Cellule cellule) {
        this.position = cellule.getPosition();
        this.statut = cellule.getStatut();
        this.couleur = cellule.getCouleur();
    }

     public Cellule(Position position, Statut statut,Couleur couleur) {
        this.position = position;
        this.statut = statut;
        this.couleur = couleur;
    }
    @Override
    public String toString() {
        return "Cellule{" + "position=" + position + ", statut=" + statut + ", couleur=" + couleur + '}';
    }
    
    public Cellule(Position position) {
        this.position = position;
        this.statut = Statut.Vide;
    }
    
    public Statut getStatut() {
        return statut;
    }
    
    public Position getPosition() {
        return position;
    }
    
    public Couleur getCouleur() {
        return couleur;
    }
    
    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }
    
    public void affecterCellule(Couleur couleur) {
        if (couleur != null) {
            this.couleur = couleur;
            this.statut = Statut.Occupe;
        } else {
            this.couleur = null;
            this.statut = Statut.Vide;
        }
    }
    
}
