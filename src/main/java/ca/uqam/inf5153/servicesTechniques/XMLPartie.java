/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.servicesTechniques;

import ca.uqam.inf5153.depotdonnees.PersistanceXML;
import ca.uqam.inf5153.domaine.jeu.Cellule;
import ca.uqam.inf5153.utils.Couleur;
import ca.uqam.inf5153.utils.Position;
import ca.uqam.inf5153.utils.Statut;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLPartie {

    private long idPartie;
    private Integer niveau;
    private Integer noCoup;
    private PersistanceXML persistance;
    private ArrayList<ArrayList<Cellule>> coupsExecutes;
    private ArrayList<Cellule> coupCellulesMAJ;
    

    public XMLPartie(Integer idPartie) {
        this();
        this.idPartie = idPartie;
        coupsExecutes = new ArrayList<ArrayList<Cellule>>();
    }

    public XMLPartie() {
        persistance = new PersistanceXML();
    }

    public void setIdPartie(long IdPartie) {
        this.idPartie = idPartie;

    }

    public Timestamp getIdPartie() {
        return new Timestamp(idPartie);
    }

    public void setXMLCoup (Integer noCoup, ArrayList<Cellule> cellules) {
        this.coupCellulesMAJ = cellules;
        this.coupsExecutes.add(cellules);
    }
    
    public void setNiveau(String niveau) {
        this.niveau = Integer.parseInt(niveau);
    }

    public int getNiveau() {
        return niveau;
    }

    public ArrayList<ArrayList<Cellule>> getCoupsExecutes() {
        return coupsExecutes;
    }

    public void setCoupsExecutes(ArrayList<ArrayList<Cellule>> coupsExecutes) {
        this.coupsExecutes = coupsExecutes;
    }
    
    // transforme une partie en un objet XML 
    public void asXML(List<List<Cellule>> coups, Timestamp idPartie, Integer niv) throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException {

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = documentFactory.newDocumentBuilder();
        Document document = parser.newDocument();

        Element racine = document.createElement("partie");
        document.appendChild(racine);

        Element eleNiveau = document.createElement("niveau");
        eleNiveau.setTextContent(niv.toString());

        racine.appendChild(eleNiveau);

        Element eleCoups = document.createElement("coups");


        for (Integer k = 0; k < coups.size(); k++) {
            Element elemCoup = document.createElement("coup");
            Element nCoup = document.createElement("noCoup");
            nCoup.setTextContent(k.toString());

            List<Cellule> tableCellule = coups.get(k);
            for (int i = 0; i < tableCellule.size(); i++) {
                String couleur;
                Cellule cell = tableCellule.get(i);
                Element eleCell = document.createElement("cellule");
                Element eleCelluleP = document.createElement("ligne");
                eleCelluleP.setTextContent(cell.getPosition().getLigneString());
                Element eleCelluleC = document.createElement("colonne");
                eleCelluleC.setTextContent(cell.getPosition().getColonneString());
                Element eleCelluleSt = document.createElement("statut");
                eleCelluleSt.setTextContent(cell.getStatut().toString());
                Element eleCelluleCoul = document.createElement("couleur");
                if (cell.getCouleur() == null) {
                    couleur = "";
                } else {
                    couleur = cell.getCouleur().toString();
                }

                eleCelluleCoul.setTextContent(couleur);

                eleCell.appendChild(eleCelluleP);
                eleCell.appendChild(eleCelluleC);
                eleCell.appendChild(eleCelluleSt);
                eleCell.appendChild(eleCelluleCoul);

                elemCoup.appendChild(eleCell);

            }
            elemCoup.appendChild(nCoup);

            eleCoups.appendChild(elemCoup);
        }

        racine.appendChild(eleCoups);

        persistance.ecrire(document, idPartie);

    }
    
    // transforme un objet XML en un objet partie
    public void asPartie(long idPartie) throws ParserConfigurationException, SAXException, IOException {
        setIdPartie(idPartie);
        Document document = persistance.lireFichierPartie(idPartie);
        String niv;
        ArrayList<ArrayList<Cellule>> coupsExecutes = new ArrayList<ArrayList<Cellule>>();

        Element racine = document.getDocumentElement();

        Element niveau = (Element) racine.getElementsByTagName("niveau").item(0);
        niv = niveau.getTextContent();
        setNiveau(niv);
        Element coups = (Element) racine.getElementsByTagName("coups").item(0);
        NodeList coup = coups.getElementsByTagName("coup");
        int nbCoup = coup.getLength();
        for (int i = 0; i < nbCoup; i++) {
            coupCellulesMAJ = new ArrayList<Cellule>();
            Element elemCoup = (Element) coup.item(i);
            NodeList cellule = elemCoup.getElementsByTagName("cellule");
            int nbCellule = cellule.getLength();
            for (int j = 0; j < nbCellule; j++) {

                Element cel = (Element) cellule.item(j);

                String ligne = getTagValue("ligne", cel);
                String colonne = getTagValue("colonne", cel);
                String statut = getTagValue("statut", cel);
                String couleur = getTagValue("couleur", cel);

                Position pos = new Position();
                pos.setPosition(Integer.parseInt(ligne), Integer.parseInt(colonne));
                
                Statut stat;
                if (statut.equals("Occupe")) {
                    stat = Statut.Occupe;
                } else {
                    stat = Statut.Vide;
                }
                
                Couleur coul;
                if (couleur.equals("Noir")) {
                    coul = Couleur.Noir;
                } else {
                    coul = Couleur.Blanc;
                }

                Cellule cellCoup = new Cellule(pos, stat, coul);
                coupCellulesMAJ.add(cellCoup);

            }
            String noCoup = getTagValue("noCoup", elemCoup);

              coupsExecutes.add(coupCellulesMAJ);

        }
        setCoupsExecutes(coupsExecutes);
    }

    private static String getTagValue(String tag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(tag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0);
        if (nValue == null) {
            return null;
        }
        return nValue.getNodeValue();
    }

}
