/*
 * Auteurs:
 * FATIMA-EZZAHRA ALLAMOU    : ALLF09628903
 * SYLVAIN DIONNE            : DIOS29066508 
 *
 */

package ca.uqam.inf5153.utils;


public class Position {
    private Integer ligne;
    private Integer colonne;
    
    public Position() {
    }
    
    public Position(Integer ligne, Integer colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }
    
    public Position getPosition() {
        return this;
    }
    
    
    public void setPosition(Integer ligne, Integer colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public int getLigne() {
        return ligne;
    }
    
    public int getColonne() {
        return colonne;
    }
    
    public boolean positionValide() {
      
        return ligne <= 8 && colonne <= 8;
    }
    public String getLigneString(){
     return ligne.toString();
    
    }
    public  String getColonneString(){
        return colonne.toString();
    }

    @Override
    public String toString() {
        return "Position{" + "ligne=" + ligne + ", colonne=" + colonne + '}';
    }
    
}
